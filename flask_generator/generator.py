import click
import os, sys, shutil


@click.command()
@click.argument('app_name')
def cli(app_name):
	'''Generates a boilerplate code for a flask app with the provided app name.'''
	cwd = os.path.dirname(__file__)
	print(cwd)
	src = os.path.join(cwd, 'template')
	dest = app_name
	if os.path.exists(dest):
		shutil.rmtree(dest)
		click.echo('Existing folder with the same name found.\
			\nRemoving the existing folder.\
			\n----------------------------------------')
	click.echo('Creating the boilerplate files...')
	shutil.copytree(src, dest)
	printsuccess(app_name, dest)


def printsuccess(app_name, dest):
	click.secho("Flask app %s boilerplate has been successfully generated in %s"
		%(app_name, os.path.join(os.getcwd(), dest)), fg="green")
	click.echo('----------------------------------------')
	click.secho('\n\nNext steps:\
		\n\n  cd %s\
		\n\n  pip install -r requirements.txt\
		\n\n  python run.py' %(dest), fg="yellow")