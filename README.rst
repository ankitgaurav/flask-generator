================
 Flask Generator
================

 This is a simple CLI utility to generate boilerplate code for flask apps.

Starting with a flask app requires one to write the same/similar boilerplate code almost on every new project. So this tool comes to the rescue of developers who either look for some repository online or have a sample scaffolding in their local machines.
The tool is intended automate creation of the required files/folders and some basic command line steps that are required in almost all python & flask projects.

Instructions
==================

Installation
------------

+ Clone the repository to your local machine
+ cd into the flask_generator directory
+ Run `pip install --editable .`

Usage
-----

+ cd into the folder where you want create your app
+ Run ``generate <app_name>`` (Note: Replace <app_name> with your project name)
+ cd into <app_name>
+ Run ``pip install -r requirements.txt``
+ Run ``python run.py``
+ Open your browser and go to ``localhost:5000``
+ Wear your developer hat and code away your awesome project now. :wink:

Requirements
============

You need to have Python 3.0+ installed in your machine that ships with pip in it by default.
This tool has been tested to work in Windows platform only as yet. Support for other OS is not guaranteed.

Credits
=======

Contributors
------------
* `Ankit Gaurav <https://twitter.com/iankitgaurav>`__

Dependencies
------------

* `Click <https://click.pocoo.org>`__ (for generating the cli interface quick and easy)
