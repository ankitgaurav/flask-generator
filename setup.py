"""Flask-Generator project"""
from setuptools import find_packages, setup

setup(
    name = 'flask_generator',
    version = '0.1',
    description = "Flask boilerplate generator",
    long_description = "A cli tool to generate boilerplate code for a flask web application with ease.",
    author="Ankit Gaurav",
    author_email="ankitgaurav@outlook.com",
    url="https://ankitgaurav.github.io/flask-generator",
    license = "MIT",
    packages =['flask_generator'],
    install_requires=[
        'Click',
    ],
    entry_points={
        'console_scripts': [
        'generate=flask_generator.generator:cli'
        ]
    },
    )